DreML: Drupal Relationship and Enitity Markup Language
======================================================

This module provides a markup of entity properties.

USAGE
=====

Install the module.
Access the url [unique-id]/[entity-type]/[entity-id] to generate the markup.

[unique-id] (optional): Required to generate unique identifier for the entity. Can be the site name.
[entity-type] : Valid entity type.
[entity-id] : Existing entity id.

  e.g. my_site/node/1, node/1

HOOKS
=====

DreML provides hook_dreml_entity_alter().
It takes two arguments: $entity_info and $entity_type to modify the entity array, for cases such as removing user mail and password from markup.

/**
 * Implements hook_dreml_entity_alter().
 */
function hook_dreml_entity_alter($entity_info, $entity_type) {
  if ($entity_type == 'user') {
    $passwd_key = array_search('pass', $entity_info['schema_fields_sql']['base table']);
    if ($passwd_key) {
      // Remove password from DreML.
      unset($entity_info['schema_fields_sql']['base table'][$passwd_key]);
    }
  }
  return $entity_info;
}